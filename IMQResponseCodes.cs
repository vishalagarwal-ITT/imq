using System;
class IMQResponseCodes {
    public static string ResponseCode200 = "200";
    public static string ResponseCode201 = "201";
    public static string ResponseCode500 = "500";
    public static string ResponseCode401 = "401";
    public static string ResponseCode409 = "409";
    public static string ResponseCode400 = "400";
}