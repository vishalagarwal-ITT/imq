using System;
using System.Net;
using System.Net.Sockets;
namespace Client {
    class ClientService {
        public static TcpClient ConnectToServer (int portAddress) {
            TcpClient tcpClient = new TcpClient ();
            try {
                tcpClient.Connect (IPAddress.Loopback, portAddress);
                return tcpClient;
            } catch {
                Console.WriteLine ("Please Check that Server is running on the port {0}", portAddress);
                return null;
            }
        }
        public void StartClient (int portAddress) {
            NetworkStream serverStream;
            string userChoice;
            try {
                TcpClient clientSocket = ConnectToServer (portAddress);
                if (null != clientSocket) {
                    while (true) {
                        userChoice = ClientHelper.SendUserChoice ();
                        if (!userChoice.Contains (DataConstant.exit)) {
                            serverStream = ClientHelper.SendDataToServer (clientSocket, userChoice);
                            userChoice = ClientHelper.ReceiveDataFromServer (serverStream);
                            if (null != userChoice) {
                                serverStream = ClientHelper.SendDataToServer (clientSocket, userChoice);
                                ClientHelper.ReceiveDataFromServer (serverStream);
                            } else {
                                Console.WriteLine (UserMessages.closeConnection);
                                return;
                            }
                        } else {
                            Console.WriteLine (UserMessages.closeConnection);
                            return;
                        }
                    }
                }
            } catch {
                Console.WriteLine (UserMessages.severError);
            }
        }
    }
}