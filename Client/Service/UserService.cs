using System;
namespace Client {
    class UserService {
        public static string RegisterNewuser () {
            Console.WriteLine ("Please Enter UserName");
            string userName = Console.ReadLine ();
            Console.WriteLine ("Please Enter Password");
            string password = Console.ReadLine ();
            Console.WriteLine ("Please Re-enter Your Password");
            string confirmPassword = Console.ReadLine ();
            if (password.Equals (confirmPassword)) {
                string userdata = DataConstant.registerConstant + userName + "+" + password;
                return userdata;
            } else {
                Console.WriteLine ("Please Start Again, Your Password Doesn't Match");
                return null;
            }
        }
        public static string LoginAsExistingUser () {
            Console.WriteLine ("Please Enter UserName");
            string userName = Console.ReadLine ();
            Console.WriteLine ("Please Enter Password");
            string password = Console.ReadLine ();
            string userdata = DataConstant.loginConstant + userName + "+" + password;
            return userdata;
        }

        public static string PerformUserOperation (string clientId) {
            Console.WriteLine ("Do You Want To Publish A Topic Or Subscribe A Topic");
            Console.WriteLine ("P For Publishing A Topic");
            Console.WriteLine ("S For Subscribing A Topic");
            Console.WriteLine ("Any Other Key For Exiting The System");
            string choice = Console.ReadLine ();
            if (choice.Equals (UserMessages.publisher)) {
                return PublisherService.RequestPublisherOperation (clientId);
            } else if (choice.Equals (UserMessages.subscriber)) {
                return SubscriberService.RequestSubscriberOperation (clientId);
            } else
                return "Exit The System";

        }
    }
}