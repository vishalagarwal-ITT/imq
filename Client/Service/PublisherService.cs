using System;
namespace Client {
    class PublisherService {
        public static string RequestPublisherOperation (string clientId) {
            Console.WriteLine ("0: Show List Of Published Topics\n1: For Show All Topic List\n2: To Publish In Topic \nAny Other Key To Exit");
            string userChoice = Console.ReadLine ();
            if (userChoice.Equals ("0")) {
                return DataConstant.publisherConstant + DataConstant.myPublishedTopic+DataConstant.stringSeparator + clientId;
            } else if (userChoice.Equals ("1")) {
                return DataConstant.publisherConstant + DataConstant.showAllTopics;
            } else if (userChoice.Equals ("2")) {
                Console.WriteLine ("Please Enter Topic Name In Which You want To Publish");
                string topicName = Console.ReadLine ();
                Console.WriteLine ("Please Enter Message That You Want To Publish");
                string message = Console.ReadLine ();
                return DataConstant.publisherConstant + DataConstant.publishMessage + DataConstant.stringSeparator + topicName + DataConstant.stringSeparator + clientId + DataConstant.stringSeparator + message;
            } else
                return null;
        }
    }
}