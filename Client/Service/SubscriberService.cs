using System;
namespace Client {
    class SubscriberService {
        public static string RequestSubscriberOperation (string clientId) {
            Console.WriteLine ("0: Topics Already Subscribed By Me\n1: For Show All Topic List\n2: To Check Messaged In Topic \nAny Other Key To Exit");
            string userChoice = Console.ReadLine ();
            if (userChoice.Equals ("0")) {
                return DataConstant.subscriberConstant + DataConstant.mySubscribedTopics + DataConstant.stringSeparator + clientId;
            } else if (userChoice.Equals ("1")) {
                return DataConstant.subscriberConstant + DataConstant.showAllTopics;
            } else if (userChoice.Equals ("2")) {
                Console.WriteLine ("Please Enter Topic Name In Which You want To Check Message");
                string topicName = Console.ReadLine ();
                return DataConstant.subscriberConstant + DataConstant.subscribeTopic + DataConstant.stringSeparator + topicName + DataConstant.stringSeparator + clientId;
            } else
                return null;
        }
    }
}