﻿using System;
using System.Net.Sockets;
using Protocol;
namespace Client {
    class ClientHelper {
      private  static int _dataLength;
        public static dynamic ReceiveDataFromServer (NetworkStream networkStream) {
            string response = ProtocolHelper.GetResponseMessage (networkStream);
            if (response.Contains (UserMessages.loginSuccessful)) {
                Console.WriteLine (UserMessages.loginSuccessful);
                var responses = response.Split (DataConstant.stringSeparator);
                var clientId = responses[1];
                var userChoice = UserService.PerformUserOperation (clientId);
                return userChoice;
            } else {
                if (response.Equals (DataConstant.exit)) {
                    networkStream.Close ();
                    return null;
                } else if (response.Contains (DataConstant.stringSeparator)) {
                    var responses = response.Split (DataConstant.stringSeparator);
                    for (int i = 0; i < responses.Length - 1; i++)
                        Console.WriteLine (responses[i]);
                    return response;
                } else {
                    Console.WriteLine (response);
                    return response;
                }
            }
        }

        public static NetworkStream SendDataToServer (TcpClient tcpClient, string data) {
            try {
                var request = IMQRequest.SendRequest (tcpClient.Client.LocalEndPoint.ToString (), tcpClient.Client.RemoteEndPoint.ToString (), data);
                byte[] userData = ProtocolHelper.SerializeObject (request);
                _dataLength = userData.Length;
                if (_dataLength > 0) {
                    NetworkStream networkStream = tcpClient.GetStream ();
                    networkStream.Write (userData, 0, _dataLength);
                    return networkStream;
                }
                return tcpClient.GetStream ();
            } catch {
                Console.WriteLine (UserMessages.closeConnection);
                return tcpClient.GetStream ();
            }
        }

        public static string SendUserChoice () {
            Console.WriteLine ("R For Register \nL For Login \nE For Exit \nPlease Enter Your Choice  ");
            string userChoice = Console.ReadLine ();
            string userData = string.Empty;
            if (userChoice.Equals (UserMessages.registerUser)) {
                userData = UserService.RegisterNewuser ();
            } else if (userChoice.Equals (UserMessages.loginUser)) {
                userData = UserService.LoginAsExistingUser ();
            } else {
                userData = DataConstant.exit;
            }
            return userData;
        }
    }
}