# IMQ

IMQ is InTimeTec Messaging Queue, that is graduation project for Learn and Code February 2020 batch.

## To Run The Project

**1. Starting the Server**

Please start the Server by using command **dotnet run** command in server directory.  

**2. Starting the Client**

Please start the Client by using command **dotnet run** command in client directory.  

You can run multiple clients by running similar command.