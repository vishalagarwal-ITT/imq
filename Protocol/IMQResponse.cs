using System;
using System.Collections.Generic;

namespace Protocol {

    [Serializable]
    class IMQResponse {
        public string ResponseCode = string.Empty;
        public string ResponseMessage = string.Empty;
        public static IMQResponse SendResponse (string responsecode, string message) {
            IMQResponse response = new IMQResponse ();
            response.ResponseCode = responsecode;
            response.ResponseMessage = message;
            return response;
        }
        public static bool ValidateHeaders (Dictionary<string, string> requestHeaders) {
            var serverAddress = requestHeaders["ServerId"];
            var serverid = serverAddress.Split (':');
            string serverId = serverid[serverid.Length - 1];
            if (serverId.Contains ("8888"))
                return true;
            else
                return false;

        }
    }
}