using System;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace Protocol {
    class ProtocolHelper {
       private static long _bufferSize = 9999;
       private static BinaryFormatter _formatter = new BinaryFormatter ();
        public static string GetResponseMessage (NetworkStream networkStream) {
            byte[] bytesArray = new byte[_bufferSize];
            int size = networkStream.Read (bytesArray);
            byte[] result = new byte[size];
            Array.Copy (bytesArray, result, size);
            var response = DeSerializeResponse (result);
            return response.ResponseMessage;
        }
        public static byte[] RequestByteArray (NetworkStream networkStream) {
            byte[] bytesArray = new byte[_bufferSize];
            int size = networkStream.Read (bytesArray);
            byte[] result = new byte[size];
            Array.Copy (bytesArray, result, size);
            return result;
        }

        public static byte[] SerializeObject (IMQRequest request) {
            using (var stream = new MemoryStream ()) {
                _formatter.Serialize (stream, request);
                return stream.ToArray ();
            }
        }
        public static byte[] SerializeObject (IMQResponse response) {
            using (var stream = new MemoryStream ()) {
                _formatter.Serialize (stream, response);
                return stream.ToArray ();
            }
        }
        public static IMQRequest DeSerializeRequest (byte[] byteArray) {
            _formatter.Binder = new DeSerializationHelper ();
            using (var stream = new MemoryStream (byteArray)) {
                var requestObject = (IMQRequest) _formatter.Deserialize (stream);
                return requestObject;
            }
        }
        public static IMQResponse DeSerializeResponse (byte[] byteArray) {
            _formatter.Binder = new DeSerializationHelper ();
            using (var stream = new MemoryStream (byteArray)) {
                var requestObject = (IMQResponse) _formatter.Deserialize (stream);
                return requestObject;
            }
        }
    }

    public sealed class DeSerializationHelper : System.Runtime.Serialization.SerializationBinder {
        public override Type BindToType (string assemblyName, string typeName) {
            Type typeToDeserialize = null;
            String currentAssembly = Assembly.GetExecutingAssembly ().FullName;
            assemblyName = currentAssembly;
            typeToDeserialize = Type.GetType (String.Format ("{0}, {1}", typeName, assemblyName));
            return typeToDeserialize;
        }
    }
}