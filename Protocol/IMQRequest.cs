using System;
using System.Collections.Generic;

namespace Protocol {
    [Serializable]
    class IMQRequest {
        public Dictionary<string, string> Headers = new Dictionary<string, string> ();
        public string ContentType = ".txt";
        public string ProtocolVersion = "1";
        public Dictionary<string, string> Body = new Dictionary<string, string> ();
        public static IMQRequest SendRequest (string ClientId, string ServerId, String Message) {
            var request = new IMQRequest ();
            request.Headers.Add ("ClientId", ClientId);
            request.Headers.Add ("ServerId", ServerId);
            request.Headers.Add ("Content-Type", request.ContentType);
            request.Body.Add ("Content", Message);
            return request;
        }
    }
}