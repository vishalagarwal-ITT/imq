using System.Collections.Generic;
using ClientMessage;
using IMQDataBase;
using Protocol;
namespace IMQServices {
    class PublisherService : ClientService {
        private static DataBaseHandler _dataBaseHandler = new DataBaseHandler ();
        public static IMQResponse PublishMessage (string topicName, string clientId, string message, IMQDataLoader objectLoader) {
            try {
                JsonMessage jsonMessage = new JsonMessage ();
                jsonMessage.CreateMessageFile (clientId, message);
                bool isTopicExists = CheckGivenTopicExists (topicName, objectLoader);
                objectLoader = new IMQDataLoader ();
                if (isTopicExists) {
                    string topicId = _dataBaseHandler.GetTopicId (topicName);
                    if (topicId != null) {
                        var isPublisherAdded = _dataBaseHandler.AddPublisherToPublisherTopicTable (clientId, topicId);
                        if (isPublisherAdded)
                            return StoreMessageInDataBase (clientId, topicId, message);
                        else
                            return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.publisherError);
                    } else
                        return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.topicFetcherror);
                } else {
                    bool topicCreated = _dataBaseHandler.AddNewTopic (topicName);
                    if (topicCreated) {
                        string topicId = _dataBaseHandler.GetTopicId (topicName);
                        if (topicId != null) {
                            var isPublisherAdded = _dataBaseHandler.AddPublisherToPublisherTopicTable (clientId, topicId);
                            if (isPublisherAdded)
                                return StoreMessageInDataBase (clientId, topicId, message);
                            else
                                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.publisherError);
                        } else
                            return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.topicFetcherror);
                    } else
                        return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.topicCreationError);
                }
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.internalServerError);
            }

        }

        public static IMQResponse ShowMyPublishedTopics (string clientid) {
            string topics = string.Empty;
            try {
                List<string> topicslist = _dataBaseHandler.GetAllPublishedTopics (clientid);
                if (topicslist.Count > 0) {
                    for (int i = 0; i < topicslist.Count; i++)
                        topics += (topicslist[i]) + DataConstant.stringSeparator;
                } else
                    topics = UserMessages.noPublishedTopics;

                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, topics);
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, UserMessages.noPublishedTopics);
            }
        }

        public static IMQResponse StoreMessageInDataBase (string clientId, string topicId, string message) {
            try {
                var isMessageAdded = _dataBaseHandler.InsertMessageTable (message);
                if (isMessageAdded) {
                    int messageId = _dataBaseHandler.GetMessageId (message);
                    if (messageId != -1) {
                        var isMessageMappedWithClient = _dataBaseHandler.InsertValueInMessageClientMapTable (messageId, clientId);
                        if (isMessageMappedWithClient) {
                            var isMessageMappedWithTopic = _dataBaseHandler.InsertValueInMessageTopicMapTable (messageId, topicId);
                            if (isMessageMappedWithTopic)
                                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode201, UserMessages.messagePublished);
                            else
                                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.messagePublishError);
                        } else
                            return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.messagePublishError);
                    } else
                        return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, UserMessages.messageNotExist);
                } else
                    return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.messagePublishError);
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.messagePublishError);
            }
        }
    }
}