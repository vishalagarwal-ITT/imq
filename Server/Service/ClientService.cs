using IMQException;
using Protocol;
namespace IMQServices {
    class ClientService {

        public static IMQResponse ShowAllTopics (IMQDataLoader dataObjectLoader) {
            try {
                string topicsList = string.Empty;
                dataObjectLoader = new IMQDataLoader ();
                if (dataObjectLoader.dataObjects.Count > 0) {
                    for (int i = 0; i < dataObjectLoader.dataObjects.Count; i++)
                        topicsList += dataObjectLoader.dataObjects[i].topicName + DataConstant.stringSeparator;
                } else
                    topicsList = UserMessages.noTopicsExist;
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, topicsList);
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode400, UserMessages.invalidData);
            }

        }
        public static bool CheckGivenTopicExists (string topicName, IMQDataLoader dataObjectLoader) {
            try {
                dataObjectLoader = new IMQDataLoader ();
                if (dataObjectLoader.dataObjects.Count > 0) {
                    for (int i = 0; i < dataObjectLoader.dataObjects.Count; i++) {
                        if (dataObjectLoader.dataObjects[i].topicName.Equals (topicName)) {
                            return true;
                        }
                    }
                }
                return false;
            } catch {
                return false;
            }
        }

    }
}