using IMQDataBase;
using Protocol;
namespace IMQServices {
    class RegistrationService {
        private static DataBaseHandler _dataBaseHandler = new DataBaseHandler ();
        public static IMQResponse RegisterNewuser (string userName, string password) {
            try {
                var isUserAdded = _dataBaseHandler.AddNewUser (userName, password);
                if (isUserAdded)
                    return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, UserMessages.registrationSuccessful);
                else
                    return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode409, UserMessages.registrationError);
            } catch  {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode409, UserMessages.registrationError);
            }
        }
    }
}