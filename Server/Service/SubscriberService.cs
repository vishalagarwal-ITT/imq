using System;
using System.Collections.Generic;
using IMQDataBase;
using Protocol;

namespace IMQServices {
    class SubscriberService : ClientService {
        private static DataBaseHandler _DataBaseHandler = new DataBaseHandler ();
        public static IMQResponse ShowMessagesFromSubscribedTopic (string topicName, IMQDataLoader dataObjectLoader) {
            try {
                string messages = string.Empty;
                dataObjectLoader = new IMQDataLoader ();
                if(dataObjectLoader.dataObjects.Count>0){
                foreach (var dataObject in dataObjectLoader.dataObjects) {
                    if (dataObject.topicName.Equals (topicName)) {
                        foreach (var message in dataObject.messages)
                            messages += message +DataConstant.stringSeparator;
                    }
                }
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, messages);}
                else{
                    return IMQResponse.SendResponse(IMQResponseCodes.ResponseCode200,UserMessages.noTopicSubscribed);
                }
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, UserMessages.noTopicSubscribed);
            }
        }

        public static IMQResponse ShowMySubscribedTopics (string clientId, IMQDataLoader _dataObjectLoader) {
            string topicsName = string.Empty;
            try {
                List<string> topicslist = _DataBaseHandler.GetAllSubscribedTopics (clientId);
                if (topicslist.Count > 0) {
                    for (int i = 0; i < topicslist.Count; i++)
                        topicsName += (topicslist[i]) + DataConstant.stringSeparator;
                } else
                    topicsName = UserMessages.noTopicSubscribed;
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, topicsName);
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, UserMessages.noTopicSubscribed);
            }
        }
        public static IMQResponse Subscriber (string topicName, string clientid, IMQDataLoader objectLoader) {
            try {
                objectLoader = new IMQDataLoader ();
                bool isTopicExists = CheckGivenTopicExists (topicName, objectLoader);
                if (isTopicExists) {
                    string topicId = _DataBaseHandler.GetTopicId (topicName);
                    if (null != topicId) {
                        var isSubscriberExists = _DataBaseHandler.VerifySubscriberExists (clientid, topicId);
                        if (isSubscriberExists) {
                            return ShowMessagesFromSubscribedTopic (topicName, objectLoader);
                        } else {
                            var isSubscriberAdded = _DataBaseHandler.AddSubscriberToSubscriberTopicTable (clientid, topicId);
                            if (isSubscriberAdded)
                                return ShowMessagesFromSubscribedTopic (topicName, objectLoader);
                            else
                                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.subscriberError);
                        }
                    } else
                        return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.topicFetcherror);
                } else {
                    return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, UserMessages.invalidTopicName);
                }
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.internalServerError);
            }
        }

    }
}