﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using IMQException;
using IMQServices;

namespace Server {
    class ServerService {
        public TcpListener ConfigureServer (int portAddress) {
            try {
                TcpListener Server = new TcpListener (IPAddress.Loopback, portAddress);
                Server.Start ();
                Console.WriteLine (UserMessages.serverStarted + portAddress);
                return Server;
            } catch (ServerException) {
                Console.WriteLine (UserMessages.serverError);
                return null;
            }
        }
        public void ThreadProcedure (object clientObject) {
            var client = (TcpClient) clientObject;
            while (client.Connected) {
                try {
                    ServerHelper.SendDataToClient (client);
                } catch  {
                    Console.WriteLine (UserMessages.disconnected);
                }
            }

        }
        public void RunServer (int portAddress) {
            TcpClient clientSocket;
            TcpListener serverSocket;
            try {
                serverSocket = ConfigureServer (portAddress);
                if (null != serverSocket) {
                    Console.WriteLine (UserMessages.acceptConnection);
                    while (true) {
                        clientSocket = serverSocket.AcceptTcpClient ();
                        ThreadPool.QueueUserWorkItem (ThreadProcedure, clientSocket);
                    }
                } else
                    throw new NullReferenceException ();
            } catch  {
                Console.WriteLine (UserMessages.connectionClosed);
            }

        }
    }
}