using IMQDataBase;
using IMQException;
using Protocol;
namespace IMQServices {
    class AuthenticationService {
        private static int invalidLoginId = -1;
        private static DataBaseHandler _dataBaseHandler = new DataBaseHandler ();
        public static IMQResponse ValidateUser (string userName, string password) {
            try {
                var isUserExists = _dataBaseHandler.CheckUserExists (userName, password);
                if (isUserExists) {
                    var clientid = _dataBaseHandler.GetClientId (userName);
                    if (invalidLoginId != clientid)
                        return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, UserMessages.loginSuccessful + clientid);
                    else
                        return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.loginError);
                } else {
                    return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode401, UserMessages.invalidLogin);
                }
            } catch  {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.invalidLogin);
            }
        }
    }
}