using System;
using System.Net.Sockets;
using IMQDataBase;
using IMQHandler;
using IMQServices;
using Protocol;
class ServerHelper {
    static DataBaseHandler dataBaseHandler = new DataBaseHandler ();
    public static IMQResponse GetDataFromClient (NetworkStream networkStream) {
        try {
            var byteData = ProtocolHelper.RequestByteArray (networkStream);
            var data = ProtocolHelper.DeSerializeRequest (byteData);
            if (IMQResponse.ValidateHeaders (data.Headers)) {
                var clientAddress = data.Headers[DataConstant.clientId];
                string[] clientId = clientAddress.Split (DataConstant.stringSeparator);
                if (!dataBaseHandler.CheckClientExists (clientId[clientId.Length - 1]))
                    dataBaseHandler.AddClient (clientId[clientId.Length - 1]);
                return PerformOperation (data.Body[DataConstant.content]);
            } else {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode400, UserMessages.invalidData);
            }
        } catch (Exception e) {
            Console.WriteLine (e.Message);
            return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode400, UserMessages.incompleteData);
        }
    }
    public static void SendDataToClient (TcpClient tcpClient) {
        try {
            if (tcpClient.Connected) {
                NetworkStream networkStream = tcpClient.GetStream ();
                var response = GetDataFromClient (networkStream);
                Byte[] sendBytes = ProtocolHelper.SerializeObject (response);
                networkStream.Write (sendBytes, 0, sendBytes.Length);
                networkStream.Flush ();
            } else {
                Console.WriteLine (UserMessages.clientNotConnected);
            }
        } catch { }
    }
    public static IMQResponse PerformOperation (string data) {
        try {
            IMQResponse response = new IMQResponse ();
            if (data.Contains (DataConstant.registerConstant)) {
                data = data.Replace (DataConstant.registerConstant, "");
                string[] userData = data.Split ("+");
                response = RegistrationService.RegisterNewuser (userData[0], userData[1]);
            } else if (data.Contains (DataConstant.loginConstant)) {
                data = data.Replace (DataConstant.loginConstant, "");
                string[] userData = data.Split ("+");
                response = AuthenticationService.ValidateUser (userData[0], userData[1]);
            } else if (data.Contains (DataConstant.publisherConstant)) {
                data = data.Replace (DataConstant.publisherConstant, "");
                response = PublisherHandler.PerformOperation (data);
            } else if (data.Contains (DataConstant.subscriberConstant)) {
                data = data.Replace (DataConstant.subscriberConstant, "");
                response = SubscriberHandler.PerformOperation (data);
            } else if (data.Contains (DataConstant.exit))
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode200, UserMessages.connectionClosed);
            return response;
        } catch {
            Console.WriteLine (UserMessages.notAbleToPerformOperation);
            return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode500, UserMessages.notAbleToPerformOperation);
        }
    }
}