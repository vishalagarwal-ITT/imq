using System;
using System.Collections.Generic;
using IMQServices;
using NUnit.Framework;

[TestFixture]
class SubscriberServiceTest {
    [TestCase ("")]
    [TestCase ("2")]
    [TestCase ("-2")]
    [TestCase ("01390")]
    [TestCase ("invalid")]
    public void ShowMessagesFromSubscribedTopic_TopicId_ValidateResult (string topicId) {
        var response = SubscriberService.ShowMessagesFromSubscribedTopic (topicId, new IMQDataLoader ());
        Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty Where As It Supposed To Contain Some Message");
        Assert.IsNotNull (response.ResponseMessage, "Response Message Is Null Where As It Supposed To Contain Some Message");
    }

    [TestCase ("technology", "1")]
    [TestCase ("", "1")]
    [TestCase ("89", "1")]
    [TestCase ("invalid", "1")]
    [TestCase ("technology", "")]
    [TestCase ("technology", "5473683")]
    [TestCase ("technology", "-9")]
    [TestCase ("technology", "invalid")]
    [TestCase ("invalid", "invalid")]
    public void Subscriber_Data_ValidateResult (string topicName, string clientid) {
        var response = SubscriberService.Subscriber (topicName, clientid, new IMQDataLoader ());
        var expectedResponseCode = new List<string> () { "200", "500" };
        Assert.IsTrue (expectedResponseCode.Contains (response.ResponseCode), $"Response Doesn't Contain The Expected Response Code Where As It Contains {response.ResponseCode}.");
        Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty Where As It Supposed To Contain Some Message");
        Assert.IsNotNull (response.ResponseMessage, "Response Message Is Null Where As It Supposed To Contain Some Message");
    }
}