using System.Collections.Generic;
using IMQServices;
using NUnit.Framework;

[TestFixture]
class AuthenticationServiceTest {
    [TestCase ("TestUser", "Test@123")]
    [TestCase ("", "Test@1122")]
    [TestCase ("TestUser1", "")]
    [TestCase ("", "")]
    [TestCase ("TestUser", "Test@123")]
    public void ShowAllTopics_RequiredData_VerifyResponse (string userName, string password) {
        var response = AuthenticationService.ValidateUser (userName, password);
        Assert.IsNotEmpty (response.ResponseCode, "Response Code Is Empty, It Must Contain Some Response Code");
        Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty, It Must Contain Some Message");
    }
}