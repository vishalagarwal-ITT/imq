using System.Collections.Generic;
using IMQServices;
using NUnit.Framework;

[TestFixture]
class RegistrationServiceTest {
    [TestCase ("TestUser", "Test@123")]
    [TestCase ("", "Test122")]
    [TestCase ("testuser", "")]
    [TestCase ("invalid", "invalid")]
    [TestCase ("TempUser", "Temp@123")]
    public void ShowAllTopics_RequiredData_VerifyResponse (string userName, string password) {
        var response = RegistrationService.RegisterNewuser (userName, password);
        Assert.IsNotEmpty (response.ResponseCode, "Response Code Is Empty, It Must Contain Some Response Code");
        Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty, It Must Contain Some Message");
    }
}