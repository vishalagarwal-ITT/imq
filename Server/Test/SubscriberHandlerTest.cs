using System.Collections.Generic;
using IMQHandler;
using NUnit.Framework;
[TestFixture]
class SubscriberHandlerTest {
    [TestCase ("ShowAllTopicsList()")]
    [TestCase ("SubscribeTopic")]
    [TestCase ("ShowMySubscribedTopics()")]
    [TestCase ("Invalid")]
    public void PerformOperation_Data_VerifyResponse (string data) {
        var response = SubscriberHandler.PerformOperation (data);
        Assert.IsNotEmpty (response.ResponseCode, "Response Code Is Empty, It Must Contain Some Response Code");
        Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty, It Must Contain Some Message");
    }

}