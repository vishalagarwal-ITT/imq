using System.Collections.Generic;
using NUnit.Framework;
using IMQHandler;
[TestFixture]
class PublisherHandlerTest{
    [TestCase("ShowAllTopicsList()")]
    [TestCase("PublisheMessage")]
    [TestCase("ShowMyPublishedTopics()")]
    [TestCase("Invalid")]
    public void PerformOperation_Data_VerifyResponse(string data){
        var response=PublisherHandler.PerformOperation(data);
       Assert.IsNotEmpty(response.ResponseCode,"Response Code Is Empty, It Must Contain Some Response Code");
      Assert.IsNotEmpty(response.ResponseMessage,"Response Message Is Empty, It Must Contain Some Message");
    }

}