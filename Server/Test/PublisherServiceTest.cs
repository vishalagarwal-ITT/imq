using System.Collections.Generic;
using IMQServices;
using NUnit.Framework;

[TestFixture]
class PublisherServiceTest {
  [TestCase ("technology", "1", "This Is Generated From Unit Test")]
  [TestCase ("invalid", "1", "This Is Generated From Unit Test")]
  [TestCase ("12", "1", "This Is Generated From Unit Test")]
  [TestCase ("technology", "-16", "This Is Generated From Unit Test")]
  [TestCase ("technology", "invalid", "This Is Generated From Unit Test")]
  [TestCase ("invalid", "invalid", "This Is Generated From Unit Test")]
  public void PublishMessage_ValidData_ReturnValidResponse (string topicName, string clientId, string message) {
    var response = PublisherService.PublishMessage (topicName, clientId, message, new IMQDataLoader ());
    var expectedResponseCode = new List<string> () { "200", "201", "500" };
    Assert.IsTrue (expectedResponseCode.Contains (response.ResponseCode), $"Response Doesn't Contain The Expected Response Code Where As It Contains {response.ResponseCode}.");
    Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty Where As It Supposed To Contain Some Message");
  }
  [TestCase("1")]
  [TestCase("invalid")]
  [TestCase("-1")]
  [TestCase(null)]
  public void ShowMyPublishedTopics_Data_verifyResponse (string clientId) {
    var response = PublisherService.ShowMyPublishedTopics (clientId);
    Assert.IsNotEmpty (response.ResponseCode, "Response Code Is Empty, It Must Contain Some Response Code");
    Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty, It Must Contain Some Message");
  }

  [TestCase ("1", "1", "This Is Generated From Unit Test")]
  [TestCase ("-1", "1", "This Is Generated From Unit Test")]
  [TestCase ("invalid", "1", "This Is Generated From Unit Test")]
  [TestCase ("1", "-1", "This Is Generated From Unit Test")]
  [TestCase ("1", "invalid", "This Is Generated From Unit Test")]
  [TestCase ("-1", "-1", "This Is Generated From Unit Test")]
  [TestCase ("invalid", "invalid", "This Is Generated From Unit Test")]
  public void StoreMessageInDataBase_ValidData_ReturnValidResponse (string clientId, string topicId, string message) {
    var response = PublisherService.StoreMessageInDataBase (clientId, topicId, message);
    var expectedResponseCode = new List<string> () { "200", "201", "500" };
    Assert.IsTrue (expectedResponseCode.Contains (response.ResponseCode), $"Response Doesn't Contain The Expected Response Code Where As It Contains {response.ResponseCode}.");
    Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty Where As It Supposed To Contain Some Message");
  }
}