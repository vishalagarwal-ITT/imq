using System.Collections.Generic;
using IMQServices;
using NUnit.Framework;

[TestFixture]
class ClientServiceTest {
    [Test]
    public void ShowAllTopics_RequiredData_VerifyResponse () {
        var response = ClientService.ShowAllTopics (new IMQDataLoader ());
        Assert.IsNotEmpty (response.ResponseCode, "Response Code Is Empty, It Must Contain Some Response Code");
        Assert.IsNotEmpty (response.ResponseMessage, "Response Message Is Empty, It Must Contain Some Message");
    }

    [TestCase ("technology")]
    [TestCase ("invalid")]
    [TestCase ("12")]
    [TestCase ("")]
    public void CheckGivenTopicExists_RequiredData_VerifyResponse (string topicName) {
        var response=ClientService.CheckGivenTopicExists(topicName, new IMQDataLoader());
        Assert.IsNotEmpty(response.ToString(),"Response Is Empty, It Must Contain Some Values");
    }
}