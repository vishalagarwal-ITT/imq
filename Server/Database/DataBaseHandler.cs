using System;
using System.Collections.Generic;
using IMQServices;
using MySql.Data.MySqlClient;
namespace IMQDataBase {
    class DataBaseHandler {
        private int hours = DataConstant.expirationHours;
        private string _databaseConnectionString = "Server=localhost;Database=imq;Uid=root;";
        private string _createUserTableCommand = @"Create Table If Not exists Users (SerialNumber int AUTO_INCREMENT, 
                                    ClientName VarChar(100) Not Null Unique, Password Varchar(100) Not Null,
                                     Primary key(SerialNumber));";
        private string _createTopicTableCommand = @"Create Table If Not exists Topic (SerialNumber int AUTO_INCREMENT, 
                                    TopicName VarChar(100) Not Null Unique, Primary Key (SerialNumber));";
        private string _createMessageTableCommand = @" Create Table If Not exists Message (id int(11) AUTO_INCREMENT, data varchar(1024) NOT NULL, 
                                    createdAt DateTime ,
                                    expiredAt DateTime , 
                                    Primary Key(id));";
        private string _createPublisherTopicMappingTableCommand = @"Create Table If Not exists publishertopicmap (id int(11) NOT NULL AUTO_INCREMENT, 
                                     clientId int(11) NOT NULL, topicId int(11) NOT NULL, primary key (id),
                                     foreign key (clientId) references users(SerialNumber),
                                      foreign key (topicId) references topic(SerialNumber));";

        private string _createMessageClientMappingTableCommand = @" Create Table If Not exists MessageClientMap  
                                    (id int(11) NOT NULL AUTO_INCREMENT, MessageId int(11) NOT NULL,
                                     clientId int(11) NOT NULL, primary key (id), foreign key(MessageId) references Message(id), 
                                     foreign key(clientid) references users(SerialNumber));";

        private string _createMessageTopicMappingTableCommand = @"Create Table If Not exists MessageTopicMap  (id int(11) NOT NULL AUTO_INCREMENT,
                                     MessageId int(11) NOT NULL, topicId int(11) NOT NULL, primary key (id), 
                                     foreign key(MessageId) references Message(id), 
                                     foreign key(topicId) references topic(SerialNumber));";

        private string _createSubscriberTopicMappingTableCommand = @"Create Table If Not exists subscribertopicmap  
                                        (id int(11) NOT NULL AUTO_INCREMENT, clientId int(11) NOT NULL, topicId int(11) NOT NULL, primary key (id), 
                                         foreign key(clientId) references users(SerialNumber), foreign key(topicId) references topic(SerialNumber));";

        private string _createClientsTableCommand = @"Create Table If Not exists Clients (id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                                                clientId int(11) NOT NULL);";

        private void CreateTable (string createTableQuery) {
            try {
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand createTable = new MySqlCommand (createTableQuery, connection);
                    createTable.ExecuteNonQuery ();
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.tableCreationExceptionMessage);
            }
        }
        private bool IsRequestSucceed (Int64 value) {
            if (1 == value)
                return true;
            else
                return false;
        }
        public bool AddClient (string clientId) {
            try {
                CreateTable (_createClientsTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand addNewClient = new MySqlCommand (@"Insert Into Clients (clientId) Values('" + clientId + "');", connection);
                    Int64 value = (Int64) addNewClient.ExecuteNonQuery ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.clientAddExceptionMessage);
                return false;
            }
        }
        public bool CheckClientExists (string clientId) {
            try {
                CreateTable (_createClientsTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand isClientExists = new MySqlCommand (@"Select count(*) From Clients where clientId= '" + clientId + "';", connection);
                    var value = (Int64) isClientExists.ExecuteScalar ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.clientExistsExceptionMessage);
                return false;
            }
        }
        public bool AddNewUser (string userName, string password) {
            try {
                CreateTable (_createUserTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand addNewUser = new MySqlCommand (@"Insert Into Users (ClientName, Password) Values('" + userName + "', '" + password + "');", connection);
                    var value = (Int64) addNewUser.ExecuteNonQuery ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.AddNewUserExceptionMessage);
                return false;
            }
        }
        public bool CheckUserExists (string userName, string password) {
            try {
                CreateTable (_createUserTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getUser = new MySqlCommand (@"Select count(*) From Users where ClientName= '" + userName + "' AND password= '" + password + "';", connection);
                    var value = (Int64) getUser.ExecuteScalar ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.userExistsExceptionMessage);
                return false;
            }
        }
        public int GetClientId (string userName) {
            try {
                CreateTable (_createUserTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getClientId = new MySqlCommand (@"Select SerialNumber From Users where ClientName= '" + userName + "';", connection);
                    int clientId = (int) getClientId.ExecuteScalar ();
                    return clientId;
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.userIdExceptionMessage);
                return -1;
            }
        }
        public bool AddNewTopic (string topicName) {
            try {
                CreateTable (_createTopicTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand addNewTopic = new MySqlCommand (@"Insert Into Topic (TopicName) Values('" + topicName + "');", connection);
                    var value = (Int64) addNewTopic.ExecuteNonQuery ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.addTopicExceptionMessage);
                return false;
            }
        }
        public string GetTopicId (string topicName) {
            try {
                CreateTable (_createTopicTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getTopicId = new MySqlCommand (@"Select SerialNumber From Topic where TopicName= '" + topicName + "';", connection);
                    int topicId = (int) getTopicId.ExecuteScalar ();
                    return topicId.ToString ();
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.topicIdExceptionMessage);
                return null;
            }
        }
        public List<string> GetAllTopics () {
            try {
                CreateTable (_createTopicTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getAllTopics = new MySqlCommand (@"Select TopicName From Topic;", connection);
                    var topicReader = getAllTopics.ExecuteReader ();
                    return GetDataFromReader (topicReader);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.fetchTopicListExceptionMessage);
                return new List<string> ();
            }
        }
        private List<string> GetDataFromReader (MySqlDataReader dataReader) {
            List<string> data = new List<string> ();
            while (dataReader.Read ()) {
                data.Add (dataReader.GetString (0));
            }
            return data;
        }
        public bool InsertMessageTable (string message) {
            try {
                CreateTable (_createMessageTableCommand);
                DateTime creationTime = DateTime.Now;
                DateTime expirationTime = creationTime.AddHours (hours);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand addMessageIntoTable = new MySqlCommand (@"Insert Into Message (data, createdAt , expiredAt)
                     Values ('" + message + "', '" + creationTime.ToString ("yyyy-MM-dd H:mm:ss") + "','" + expirationTime.ToString ("yyyy-MM-dd H:mm:ss") + "');", connection);
                    int value = addMessageIntoTable.ExecuteNonQuery ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.publishMessageExceptionMessage);
                return false;
            }
        }
        public int GetMessageId (string message) {
            try {
                CreateTable (_createMessageTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getMessageId = new MySqlCommand (@"Select id From Message where data= '" + message + "';", connection);
                    int messageId = (int) getMessageId.ExecuteScalar ();
                    return messageId;
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.fetchMessageIdExceptionMessage);
                return -1;
            }
        }
        public bool AddPublisherToPublisherTopicTable (string clientId, string topicId) {
            try {
                CreateTable (_createPublisherTopicMappingTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand addValueInPublisherTopicTable = new MySqlCommand (@"
                                   insert into publishertopicmap (clientId, topicId) 
                                   values ('" + clientId + "', '" + topicId + "' );", connection);
                    Int64 value = (Int64) addValueInPublisherTopicTable.ExecuteNonQuery ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.addValueInPublisherTopicExceptionMessage);
                return false;
            }
        }
        public bool InsertValueInMessageClientMapTable (int messageId, string clientId) {
            try {
                CreateTable (_createMessageClientMappingTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand addValueInMessageClientTable = new MySqlCommand (@"
                                   insert into MessageClientMap (MessageId, clientId) 
                                   values ('" + messageId + "', '" + clientId + "' );", connection);
                    var value = (Int64) addValueInMessageClientTable.ExecuteNonQuery ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.addValueInMessageClientExceptionMessage);
                return false;
            }
        }
        public bool InsertValueInMessageTopicMapTable (int messageId, string topicId) {
            try {
                CreateTable (_createMessageTopicMappingTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand addDataInMessageTopicTable = new MySqlCommand (@"
                                   insert into MessageTopicMap (MessageId, topicId)
                                    values ('" + messageId + "', '" + topicId + "' );", connection);
                    var value = (Int64) addDataInMessageTopicTable.ExecuteNonQuery ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {;
                Console.WriteLine (UserMessages.addValueInMessageTopicExceptionMessage);
                return false;
            }
        }
        public bool AddSubscriberToSubscriberTopicTable (string clientId, string topicId) {
            try {
                CreateTable (_createSubscriberTopicMappingTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand addDataInSubscriberTopicTable = new MySqlCommand (@"
                                   insert into subscribertopicmap (clientId, topicId) values
                                    ('" + clientId + "', '" + topicId + "' );", connection);
                    var value = (Int64) addDataInSubscriberTopicTable.ExecuteNonQuery ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.addValueInSubscriberTopicExceptionMessage);
                return false;
            }
        }
        public List<string> GetAllSubscribedTopics (string clientId) {
            try {
                CreateTable (_createSubscriberTopicMappingTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getSubscribedTopics = new MySqlCommand (@"
                    select topicname from topic where serialnumber in 
                                   (Select topicId From subscribertopicmap where clientId='" + clientId + "');", connection);
                    var topicReader = getSubscribedTopics.ExecuteReader ();
                    return GetDataFromReader (topicReader);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.fetchTopicsForSubscriberExceptionMessage);
                return new List<string> ();
            }
        }
        public bool VerifySubscriberExists (string clientId, string topicId) {
            try {
                CreateTable (_createSubscriberTopicMappingTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getSubscriber = new MySqlCommand (@"
                                 Select count(*) From subscribertopicmap where clientId='" + clientId +
                        "' and topicid= '" + topicId + "';", connection);
                    var value = (Int64) getSubscriber.ExecuteScalar ();
                    return IsRequestSucceed (value);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.checkSubscriberExistsExceptionMessage);
                return false;
            }
        }
        public List<string> GetAllPublishedTopics (string clientid) {
            try {
                CreateTable (_createPublisherTopicMappingTableCommand);
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getPublishedTopics = new MySqlCommand (@"
                    select topicName from topic where serialnumber in (
                    Select topicid from publishertopicmap where clientid='" + clientid + "');", connection);
                    var topicReader = getPublishedTopics.ExecuteReader ();
                    return GetDataFromReader (topicReader);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.fetchTopicListExceptionMessage);
                return new List<string> ();
            }
        }
        public List<string> GetActiveMessagesFromTopicName (string topicName) {
            try {
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getActiveMessages = new MySqlCommand (@"select data from message where expiredAt> now()    and id in (select messageid from messagetopicmap where topicid in (select serialnumber from topic where topicname='" + topicName + "'))",
                        connection);
                    var messageReader = getActiveMessages.ExecuteReader ();
                    return GetDataFromReader (messageReader);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.fetchActiveMessageExceptionMessage);
                return new List<string> ();
            }
        }
        public List<string> GetDeadMessagesFromTopicName (string topicName) {
            try {
                using (MySqlConnection connection = new MySqlConnection (_databaseConnectionString)) {
                    connection.Open ();
                    MySqlCommand getDeadMessages = new MySqlCommand (@"select data from message
                     where expiredAt>now() and id in (select messageid from messagetopicmap where topicid in (select serialnumber from topic where topicname='" + topicName + "'))",
                        connection);
                    var messageReader = getDeadMessages.ExecuteReader ();
                    return GetDataFromReader (messageReader);
                }
            } catch (MySqlException) {
                Console.WriteLine (UserMessages.fetchExpiredMessageExceptionMessage);
                return new List<string> ();
            }
        }
    }
}