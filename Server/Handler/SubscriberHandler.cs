using IMQServices;
using Protocol;

namespace IMQHandler {
    class SubscriberHandler {
        public static IMQResponse PerformOperation (string data) {
            try {
                IMQResponse response = new IMQResponse ();
                IMQDataLoader _dataObjectLoader = new IMQDataLoader ();
                if (data.Contains (DataConstant.showAllTopics)) {
                    response = SubscriberService.ShowAllTopics (_dataObjectLoader);
                } else if (data.Contains (DataConstant.subscribeTopic)) {
                    string[] userinfo = data.Split (DataConstant.stringSeparator);
                    string topicName = userinfo[1];
                    string clientId = userinfo[2];
                    response = SubscriberService.Subscriber (topicName, clientId, _dataObjectLoader);
                } else if (data.Contains (DataConstant.mySubscribedTopics)) {
                    string[] userinfo = data.Split (DataConstant.stringSeparator);
                    string clientid = userinfo[userinfo.Length - 1];
                    response = SubscriberService.ShowMySubscribedTopics (clientid, _dataObjectLoader);
                }
                return response;
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode400, UserMessages.incompleteData);
            }
        }
    }
}