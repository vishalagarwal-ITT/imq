using IMQServices;
using Protocol;

namespace IMQHandler {
    class PublisherHandler {
        public static IMQResponse PerformOperation (string data) {
            try {
                IMQResponse response = new IMQResponse ();
                IMQDataLoader _dataObjectLoader = new IMQDataLoader ();
                if (data.Contains (DataConstant.showAllTopics)) {
                    response = PublisherService.ShowAllTopics (_dataObjectLoader);
                } else if (data.Contains (DataConstant.publishMessage)) {
                    string[] userinfo = data.Split (DataConstant.stringSeparator);
                    string topicName = userinfo[1];
                    string clientId = userinfo[2];
                    string userMessage = userinfo[3];
                    response = PublisherService.PublishMessage (topicName, clientId, userMessage, _dataObjectLoader);
                } else if (data.Contains (DataConstant.myPublishedTopic)) {
                    string[] userinfo = data.Split (DataConstant.stringSeparator);
                    string clientid = userinfo[userinfo.Length - 1];
                    response = PublisherService.ShowMyPublishedTopics (clientid);
                } else {
                    response = IMQResponse.SendResponse (IMQResponseCodes.ResponseCode400, UserMessages.invalidData);
                }
                return response;
            } catch {
                return IMQResponse.SendResponse (IMQResponseCodes.ResponseCode400, UserMessages.incompleteData);
            }
        }
    }
}