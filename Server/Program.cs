using System;
using System.Text;
using ClientMessage;
using IMQDataBase;
using Server;
namespace ServerProgram {
  class Program {
    public static void Main (string[] args) {
     ServerService serverService = new ServerService ();
     serverService.RunServer (DataConstant.portAddress);  
    }
  }
}