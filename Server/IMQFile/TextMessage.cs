using System;
using System.IO;
namespace ClientMessage {
    class TextMessage : IMessageFactory {
        int hours = DataConstant.expirationHours;
        string DirectoryPath = "Client";
        public void CreateMessageFile (string clientId, string Data) {
            string FilePath = GetTextFilePath (clientId);
            string[] stringdata = {
                "messageCreationTime = " + DateTime.Now.ToString (),
                "messageExpirationTime = " + DateTime.Now.AddHours (hours).ToString (),
                "messageData = " + Data
            };
            File.AppendAllLines (FilePath, stringdata);
        }
        private string GetTextFilePath (string clientId) {
            CreateDirectoryToStoreFile ();
            var dateTime = DateTime.Now.ToString ("yyyy/MM/dd__HH/mm/ss/fff");
            string FileName = $"\\{DirectoryPath}" + clientId + "_" + dateTime + ".txt";
            string FilePath = Path.Combine (DirectoryPath + FileName);
            FilePath = FilePath.Replace (":", "_");
            return FilePath;
        }
        public void CreateDirectoryToStoreFile () {
            try {
                if (!Directory.Exists (DirectoryPath)) {
                    var FolderName = Directory.CreateDirectory (DirectoryPath);
                }
            } catch {
                Console.WriteLine ("Not Able To Create Directory");
            }
        }
    }
}