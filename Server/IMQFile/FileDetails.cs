using System;
[Serializable]
class FileDetails {
    public string messageCreationTime { get; set; }
    public string messageExpirationTime { get; set; }
    public string messageData { get; set; }
}