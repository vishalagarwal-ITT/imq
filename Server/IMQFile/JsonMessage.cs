using System;
using System.IO;
using IMQException;
using Newtonsoft.Json;
namespace ClientMessage {
    class JsonMessage : IMessageFactory {
        int hours = DataConstant.expirationHours;
        string DirectoryPath = "Client";
        public void CreateMessageFile (string clientId, string Data) {
            string FilePath = GetJSONFilePath (clientId);
            FileDetails details = new FileDetails () {
                messageCreationTime = DateTime.Now.ToString (),
                messageExpirationTime = DateTime.Now.AddHours (hours).ToString (),
                messageData = Data
            };
            var stringmessage = JsonConvert.SerializeObject (details);
            File.WriteAllText (FilePath, stringmessage);
        }
        private string GetJSONFilePath (string clientId) {
            CreateDirectoryToStoreFile ();
            try {
                var dateTime = DateTime.Now.ToString ("yyyy/MM/dd__HH/mm/ss/fff");
                string FileName = $"\\{DirectoryPath}" + clientId + "_" + dateTime + ".json";
                string FilePath = Path.Combine (DirectoryPath + FileName);
                FilePath = FilePath.Replace (":", "_");
                return FilePath;
            } catch (ServerException) {
                return null;
            }
        }
        public void CreateDirectoryToStoreFile () {
            try {
                if (!Directory.Exists (DirectoryPath)) {
                    var FolderName = Directory.CreateDirectory (DirectoryPath);
                }
            } catch (ServerException) {
                Console.WriteLine ("Can't Create Directory");
            }
        }
    }
}