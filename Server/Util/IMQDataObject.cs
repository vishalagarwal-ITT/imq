using System.Collections.Generic;
class IMQDataObject {
    public string topicName;
    public List<string> messages;
    public List<string> deadMessages;
    public IMQDataObject (string topic, List<string> message, List<string> deadMessage) {
        topicName = topic;
        messages = message;
        deadMessages = deadMessage;
    }
}