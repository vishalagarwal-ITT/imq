using System.Collections.Generic;
using IMQDataBase;
class IMQDataLoader {
  DataBaseHandler _DataBaseHandler = new DataBaseHandler ();
  public List<IMQDataObject> dataObjects;
  public IMQDataLoader () {
    var allTopicsName = _DataBaseHandler.GetAllTopics ();
    dataObjects = new List<IMQDataObject> ();
    foreach (var topic in allTopicsName) {
      var messges = _DataBaseHandler.GetActiveMessagesFromTopicName (topic);
      var deadMessage = _DataBaseHandler.GetDeadMessagesFromTopicName (topic);
      IMQDataObject dataObject = new IMQDataObject (topic, messges, deadMessage);
      dataObjects.Add (dataObject);
    }
  }
}