using System;
class DataConstant{
    public static int expirationHours=10;
    public static string showAllTopics="ShowAllTopicsList()";
    public static string publishMessage="PublisheMessage";
    public static string stringSeparator=":";
    public static string myPublishedTopic="ShowMyPublishedTopics()";
    public static string subscribeTopic="SubscribeTopic";
    public static string mySubscribedTopics="ShowMySubscribedTopics";
    public static string clientId="ClientId";
    public static string content="Content";
    public static string registerConstant="$Register";
    public static string loginConstant="$Login";
    public static string publisherConstant="@Publisher";
    public static string subscriberConstant="#Subscriber";
    public static string exit="Exit";   
    public static int portAddress=8888;
}